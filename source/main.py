#!/usr/bin/env python

from aiohttp import web
from routes import setup_routes
from db import setup_db


@web.middleware
async def middleware(request, handler):
    token = request.headers.get('X-Auth-Token', '')
    if not token:
        raise web.HTTPUnauthorized()
    response = await handler(request)
    return response


async def init_app():
    app = web.Application(middlewares=[middleware])
    setup_routes(app)
    return app


def main():
    app = init_app()
    web.run_app(app)
    return app


if __name__ == '__main__':
    main()