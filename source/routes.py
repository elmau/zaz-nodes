#!/usr/bin/env python

from views import nodes, nodes_limit


def setup_routes(app):
    app.router.add_get('/nodes', nodes)
    app.router.add_get(r'/nodes/{count:[1-9]\d{0,2}}', nodes_limit)
    return
