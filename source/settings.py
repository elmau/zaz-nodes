#!/usr/bin/env python

import logging
from conf import *


LOG_FORMAT = '%(asctime)s - %(levelname)s - %(message)s'
LOG_DATE = '%d/%m/%Y %H:%M:%S'
logging.addLevelName(logging.ERROR, '\033[1;41mERROR\033[1;0m')
logging.addLevelName(logging.DEBUG, '\x1b[33mDEBUG\033[1;0m')
logging.addLevelName(logging.INFO, '\x1b[32mINFO\033[1;0m')
if DEBUG:
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT, datefmt=LOG_DATE)
else:
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT, datefmt=LOG_DATE)
log = logging.getLogger(__name__)


HTTPS = 'https://{}'
SERVER_BMAS = f'BMAS {{}} {PORT}'
SERVER_GVA = f'GVA S {{}} {PORT} gva'


QUERY_USERNAME = """{{
    idty(pubkey: "{}") {{
      isMember
      username
    }}
}}
"""