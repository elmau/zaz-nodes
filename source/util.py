#!/usr/bin/env python

import threading
from typing import Any, Union

import mailbox
import smtplib
from smtplib import SMTPException, SMTPAuthenticationError
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders

from settings import log, TIMEOUT


class Email():
    """Class for send email
    """
    class SmtpServer(object):

        def __init__(self, config):
            self._server = None
            self._error = ''
            self._sender = ''
            self._is_connect = self._login(config)

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_value, traceback):
            self.close()

        @property
        def is_connect(self):
            return self._is_connect

        @property
        def error(self):
            return self._error

        def _login(self, config):
            name = config['server']
            port = config['port']
            is_ssl = config['ssl']
            starttls = config.get('starttls', False)
            self._sender = config['user']
            try:
                if starttls:
                    self._server = smtplib.SMTP(name, port, timeout=TIMEOUT)
                    self._server.ehlo()
                    self._server.starttls()
                    self._server.ehlo()
                elif is_ssl:
                    self._server = smtplib.SMTP_SSL(name, port, timeout=TIMEOUT)
                    self._server.ehlo()
                else:
                    self._server = smtplib.SMTP(name, port, timeout=TIMEOUT)

                self._server.login(self._sender, config['password'])
                msg = 'Connect to: {}'.format(name)
                log.debug(msg)
                return True
            except smtplib.SMTPAuthenticationError as e:
                if '535' in str(e):
                    self._error = _('Incorrect user or password')
                    return False
                if '534' in str(e) and 'gmail' in name:
                    self._error = _('Allow less secure apps in GMail')
                    return False
            except smtplib.SMTPException as e:
                self._error = str(e)
                return False
            except Exception as e:
                self._error = str(e)
                return False
            return False

        def _body_validate(self, msg):
            body = msg.replace('\n', '<BR>')
            return body

        def send(self, message):
            email = MIMEMultipart()
            email['From'] = self._sender
            email['To'] = message['to']
            email['Cc'] = message.get('cc', '')
            email['Subject'] = message['subject']
            email['Date'] = formatdate(localtime=True)
            if message.get('confirm', False):
                email['Disposition-Notification-To'] = email['From']
            if 'body_text' in message:
                email.attach(MIMEText(message['body_text'], 'plain', 'utf-8'))
            if 'body' in message:
                body = self._body_validate(message['body'])
                email.attach(MIMEText(body, 'html', 'utf-8'))

            paths = message.get('files', ())
            if isinstance(paths, str):
                paths = (paths,)
            for path in paths:
                fn = Paths(path).file_name
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(Paths.read_bin(path))
                encoders.encode_base64(part)
                part.add_header(
                    'Content-Disposition', f'attachment; filename="{fn}"')
                email.attach(part)

            receivers = (
                email['To'].split(',') +
                email['CC'].split(',') +
                message.get('bcc', '').split(','))
            try:
                self._server.sendmail(self._sender, receivers, email.as_string())
                msg = 'Email sent...'
                log.info(msg)
                if message.get('path', ''):
                    self.save_message(email, message['path'])
                return True
            except Exception as e:
                self._error = str(e)
                return False
            return False

        def save_message(self, email, path):
            mbox = mailbox.mbox(path, create=True)
            mbox.lock()
            try:
                msg = mailbox.mboxMessage(email)
                mbox.add(msg)
                mbox.flush()
            finally:
                mbox.unlock()
            return

        def close(self):
            try:
                self._server.quit()
                msg = 'Close connection...'
                log.debug(msg)
            except:
                pass
            return

    @classmethod
    def _send_email(cls, server, messages):
        with cls.SmtpServer(server) as server:
            if server.is_connect:
                for msg in messages:
                    server.send(msg)
            else:
                log.error(server.error)
        return server.error

    @classmethod
    def send(cls, server: dict, messages: Union[dict, tuple, list]):
        """Send email with config server, emails send in thread.

        :param server: Configuration for send emails
        :type server: dict
        :param messages: Dictionary con message or list of messages
        :type messages: dict or iterator
        """
        if isinstance(messages, dict):
            messages = (messages,)
        t = threading.Thread(target=cls._send_email, args=(server, messages))
        t.start()
        return


def send_email(server, message):
    Email.send(server, message)
    return
