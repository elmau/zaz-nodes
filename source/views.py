#!/usr/bin/env python

from aiohttp import web
import db


async def nodes(request):
    data = await db.nodes_all()
    return web.json_response(data)


async def nodes_limit(request):
    count = int(request.match_info['count'])
    data = await db.nodes_limit(count)
    return web.json_response(data)
