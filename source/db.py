#!/usr/bin/env python

import argparse
import asyncio
import time
from pathlib import Path
from urllib.request import urlopen

# ~ import aiohttp
import peewee
from peewee_aio import Manager, AIOModel, fields

from duniterpy.api.client import Client
from duniterpy.helpers import network
from graphql import build_client_schema, get_introspection_query, language, validate
from graphql.error import GraphQLSyntaxError

from settings import (log,
    EMAIL_ADMIN,
    HTTPS,
    SERVER_BMAS,
    SERVER_GVA,
    SERVER_STMP,
    QUERY_USERNAME,
    TIMEOUT,
    MY_NODES)
from util import send_email


NAME_DB = 'nodes.db'
path_db = f'sqlite:///{Path(__file__).parent}/{NAME_DB}'
manager = Manager(path_db)


@manager.register
class Node(AIOModel):
    id = fields.AutoField()
    pubkey = fields.CharField(unique=True)
    active = fields.BooleanField(default=False)
    last_block = fields.IntegerField(null=True)
    latency = fields.IntegerField(null=True)
    version = fields.CharField(null=True)
    endpoints = fields.CharField(null=True)
    url = fields.CharField(null=True)
    username = fields.CharField(null=True)
    is_member = fields.BooleanField(default=False)


FIELDS = (
    Node.username,
    Node.last_block,
    Node.url,
    Node.latency,
)
WHERE = ((Node.active == True) & (Node.latency > 0))


async def nodes_all():
    async with manager.connection():
        nodes = await (Node
            .select(*FIELDS)
            .where(WHERE)
            .order_by(+Node.latency)
            .tuples())
    return nodes


async def nodes_limit(count):
    async with manager.connection():
        nodes = await (Node
            .select(*FIELDS)
            .where(WHERE)
            .limit(count)
            .order_by(+Node.latency)
            .tuples())
    return nodes


async def gva_query_user(server, pubkey):
    try:
        client = Client(SERVER_GVA.format(server))
        query = QUERY_USERNAME.format(pubkey)
        user = client.query(query)['data']['idty']
        return user
    except Exception as e:
        log.error(e)
        return False


def _get_url(endpoints):
    url = ''
    first = endpoints[0]
    parts = first.split(' ')
    index = 1
    if parts[0] == 'GVA':
        index = 2
    url = parts[index]
    return url


def _get_latency(url, endpoints=''):
    latency = 0
    try:
        start = time.time()
        res = urlopen(HTTPS.format(url), timeout=TIMEOUT).read()
        latency = int((time.time() - start) * 1000)
    except Exception as e:
        info_error = f'URL: {url}\n\t{e}\n\t'
        if endpoints:
            info_error += endpoints
        log.error(info_error)
        if url in MY_NODES:
            message = dict(
                to = EMAIL_ADMIN,
                subject = f'Error en nodo: {url}',
                body = info_error,
            )
            send_email(SERVER_STMP, message)
    return latency


def _get_node_alive():
    for server in MY_NODES:
        log.info(f'Alive: {server}')
        latency = _get_latency(server)
        if latency == 0:
            continue
        client = Client(SERVER_BMAS.format(server))
        groups = network.get_available_nodes(client)
        if len(groups[0]) == 1:
            message = dict(
                to = EMAIL_ADMIN,
                subject = f'Error en nodo: {server}',
                body = 'Parece que el nodo esta desincronizado...',
            )
            send_email(SERVER_STMP, message)
            continue
        else:
            return server, groups[0]
    return '', []


async def get_nodes():
    log.info('Updating nodes...')
    server, groups = _get_node_alive()
    if not server:
        return

    async with manager.connection():
        await Node.update({'active': False})
        for group in groups:
            head = group['head']
            pubkey = head.pubkey
            node, created = await Node.get_or_create(pubkey=pubkey)
            node.last_block = head.block_id.number
            node.active = True
            node.endpoints = '|'.join(group['endpoints'])
            node.url = _get_url(group['endpoints'])
            node.latency = _get_latency(node.url, node.endpoints)
            if created:
                node.version = head.software_version
                user = await gva_query_user(server, pubkey)
                if user:
                    node.is_member = user['isMember']
                    node.username = user['username']
            elif node.username is None:
                user = await gva_query_user(server, pubkey)
                if user:
                    node.is_member = user['isMember']
                    node.username = user['username']
            await node.save()

    log.info('Nodes updated...')


async def setup_db():
    async with manager:
        async with manager.connection():
            await Node.create_table()
            await get_nodes()


def _process_command_line_arguments():
    parser = argparse.ArgumentParser(description='Nodes DB')
    parser.add_argument('-c', '--create', action='store_true')
    args = parser.parse_args()
    return args


def main(args):
    if args.create:
        asyncio.run(setup_db())
    else:
        asyncio.run(get_nodes())
    return


if __name__ == '__main__':
    args = _process_command_line_arguments()
    main(args)
